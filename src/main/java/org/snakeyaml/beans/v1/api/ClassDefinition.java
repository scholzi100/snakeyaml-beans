/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.api;

import org.snakeyaml.beans.v1.exceptions.YamlInstanceCreationException;
import org.snakeyaml.engine.v2.api.ConstructNode;
import org.snakeyaml.engine.v2.nodes.Node;

import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Provides additional runtime information necessary to create a custom Java instance
 */
public final class ClassDefinition {
    private static final Logger log = Logger.getLogger(ClassDefinition.class.getPackage().getName());

    /**
     * The class of the JavaBean for this ClassDefinition
     */
    private final Class<? extends Object> type;

    public ClassDefinition(Class<? extends Object> type) {
        this.type = type;
    }

    /**
     * Get represented type (class)
     * @return type (class) to be described.
     */
    public Class<? extends Object> getType() {
        return type;
    }

    /**
     * This method should be overridden for ClassDefinition implementations that are supposed to implement
     * instantiation logic that is different from default one as implemented in YAML constructors.
     * Note that even if you override this method, default filling of fields with
     * variables from parsed YAML will still occur later.
     *
     * @param node - node to construct the instance from
     * @return new instance
     */
    public Object newInstance(Node node) {
        try {
            java.lang.reflect.Constructor<?> c = type.getDeclaredConstructor();
            c.setAccessible(true);
            return c.newInstance();
        } catch (Exception e) {
            log.fine(e.getLocalizedMessage());
            throw new YamlInstanceCreationException("Could not create empty instance of " + type.toString(),
                    node.getStartMark());
        }
    }

    /**
     * Get the ConstructNode which should be used to create an instance of the class which is specified for
     * this ClassDefinition
     * @param node - the first node to create an instance for
     * @return ConstructNode or Optional.empty() if no custom ConstructNode should be used
     */
    public Optional<ConstructNode> getConstructNode(Node node) {
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassDefinition that = (ClassDefinition) o;
        return Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }
}
