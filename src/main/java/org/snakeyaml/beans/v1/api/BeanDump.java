/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.api;

import org.snakeyaml.beans.v1.introspector.PropertyUtils;
import org.snakeyaml.beans.v1.representer.JavaBeanRepresenter;
import org.snakeyaml.engine.v2.api.Dump;
import org.snakeyaml.engine.v2.api.DumpSettings;

import java.util.HashMap;

public class BeanDump extends Dump {

    public BeanDump(DumpSettings settings) {
        super(settings);
        this.representer = new JavaBeanRepresenter(new PropertyUtils(),
                new ClassDefinitionRegistry(new HashMap<>()),
                settings);
    }
}




