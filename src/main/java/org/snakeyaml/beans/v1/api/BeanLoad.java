/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.api;

import org.snakeyaml.beans.v1.contruct.JavaBeanConstructor;
import org.snakeyaml.engine.v2.api.Load;
import org.snakeyaml.engine.v2.api.LoadSettings;
import org.snakeyaml.engine.v2.composer.Composer;
import org.snakeyaml.engine.v2.nodes.Node;

import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

/**
 * Common way to load custom Java instance of the specified class
 */
public class BeanLoad<T> extends Load {

    private final JavaBeanConstructor<T> javaBeanConstructor;

    /**
     * Create instance to parse the incoming YAML data and create Java instances
     *
     * @param settings - configuration
     * @param rootContext  - the context for the class of the instance to be loaded
     */
    public BeanLoad(LoadSettings settings, ClassDefinition rootContext) {
        super(settings);
        Objects.requireNonNull(rootContext, "Class cannot be null");
        this.javaBeanConstructor = new JavaBeanConstructor(settings, rootContext, new ClassDefinitionRegistry(new HashMap<>()));
    }

    /**
     * Parse a YAML document and create a Java instance of the specified class
     *
     * @param yaml - YAML data to load from (BOM must not be present)
     * @return parsed Java instance
     * @throws org.snakeyaml.engine.v2.exceptions.YamlEngineException if the YAML is not valid
     */
    public T loadBeanFromString(String yaml) {
        Objects.requireNonNull(yaml, "String cannot be null");
        Composer composer = createComposer(yaml);
        return loadBeanFromComposer(composer);
    }

    /**
     * Parse a YAML document and create a Java instance of the specified class
     *
     * @param input - YAML data to load from (BOM may not be present)
     * @return parsed Java instance
     * @throws org.snakeyaml.engine.v2.exceptions.YamlEngineException if the YAML is not valid
     */
    public T loadBeanFromInputStream(InputStream input) {
        Objects.requireNonNull(input, "InputStream cannot be null");
        Composer composer = createComposer(input);
        return loadBeanFromComposer(composer);
    }

    /**
     * Parse a YAML document and create a Java instance of the specified class
     *
     * @param input - YAML data to load from (BOM must not be present)
     * @return parsed Java instance
     * @throws org.snakeyaml.engine.v2.exceptions.YamlEngineException if the YAML is not valid
     */
    public T loadBeanFromReader(Reader input) {
        Objects.requireNonNull(input, "Reader cannot be null");
        Composer composer = createComposer(input);
        return loadBeanFromComposer(composer);
    }

    protected T loadBeanFromComposer(Composer composer) {
        Optional<Node> nodeOptional = composer.getSingleNode();
        return javaBeanConstructor.constructBeanDocument(nodeOptional);
    }
}




