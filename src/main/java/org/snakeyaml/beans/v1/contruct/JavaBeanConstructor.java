/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.contruct;

import org.snakeyaml.beans.v1.api.ClassDefinition;
import org.snakeyaml.beans.v1.api.ClassDefinitionRegistry;
import org.snakeyaml.beans.v1.exceptions.YamlInstanceCreationException;
import org.snakeyaml.beans.v1.introspector.Property;
import org.snakeyaml.beans.v1.introspector.PropertyUtils;
import org.snakeyaml.engine.v2.api.ConstructNode;
import org.snakeyaml.engine.v2.api.LoadSettings;
import org.snakeyaml.engine.v2.constructor.StandardConstructor;
import org.snakeyaml.engine.v2.exceptions.ConstructorException;
import org.snakeyaml.engine.v2.exceptions.DuplicateKeyException;
import org.snakeyaml.engine.v2.nodes.MappingNode;
import org.snakeyaml.engine.v2.nodes.Node;
import org.snakeyaml.engine.v2.nodes.NodeTuple;
import org.snakeyaml.engine.v2.nodes.NodeType;
import org.snakeyaml.engine.v2.nodes.ScalarNode;
import org.snakeyaml.engine.v2.nodes.Tag;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class JavaBeanConstructor<T> extends StandardConstructor {

    public static final String DEFINITION_KEY = "SnakeYAML-class-definition";
    public static final String ERROR_PREFIX = "Cannot create ";

    private final LoadSettings beanLoadSettings;
    private final ClassDefinition rootContext;
    private final ClassDefinitionRegistry classDefinitionRegistry;

    public JavaBeanConstructor(LoadSettings settings, ClassDefinition rootContext, ClassDefinitionRegistry classDefinitionRegistry) {
        super(settings);
        this.beanLoadSettings = settings;
        this.rootContext = rootContext;
        this.classDefinitionRegistry = classDefinitionRegistry;
    }

    @Override
    protected Optional<ConstructNode> findConstructorFor(Node node) {
        ClassDefinition definition = (ClassDefinition) node.getProperty(DEFINITION_KEY);

        if (definition != null) {
            switch (node.getNodeType()) {
                case SCALAR:
                    return Optional.of(new ConstructScalar(definition));
                case MAPPING:
                    return Optional.of(new ConstructMapping(definition));
                case SEQUENCE:
                    //TODO sequence
                    throw new UnsupportedOperationException("Sequence is yet to be supported.");
                default:
                    throw new YamlInstanceCreationException("Unexpected Node type: " + node.getNodeType(),
                            node.getStartMark());
            }
        } else {
            return super.findConstructorFor(node);
        }
    }

    public T constructBeanDocument(Optional<Node> optionalNode) {
        optionalNode.ifPresent(node -> node.setProperty(DEFINITION_KEY, rootContext));
        return (T) super.constructSingleDocument(optionalNode);
    }

    /**
     * Construct scalar instance when the runtime class is known. Recursive structures are not supported.
     * (not applicable for scalars)
     */
    protected class ConstructScalar implements ConstructNode {
        private final ClassDefinition rootContext;

        public ConstructScalar(ClassDefinition rootContext) {
            this.rootContext = rootContext;
        }

        public Object construct(Node nnode) {
            ScalarNode scalarNode = (ScalarNode) nnode;
            Class<?> type = rootContext.getType();
            Optional<ConstructNode> contextConstruct = rootContext.getConstructNode(nnode);
            if (contextConstruct.isPresent()) {
                return contextConstruct.get().construct(scalarNode);
            } else {
                // standard classes created directly
                return constructStandardJavaInstance(type, scalarNode);
            }
        }

        //TODO make it possible to configure/redefine
        private Object constructStandardJavaInstance(Class type, ScalarNode node) {
            Object result;
            if (node.getTag() == Tag.NULL) {
                ConstructNode stringConstructor = tagConstructors.get(Tag.NULL);
                result = stringConstructor.construct(node);
            } else if (type == String.class) {
                ConstructNode stringConstructor = tagConstructors.get(Tag.STR);
                result = stringConstructor.construct(node);
            } else if (type == Boolean.class || type == Boolean.TYPE) {
                ConstructNode boolConstructor = tagConstructors.get(Tag.BOOL);
                result = boolConstructor.construct(node);
            } else if (type == Character.class || type == Character.TYPE) {
                ConstructNode charConstructor = tagConstructors.get(Tag.STR);
                String ch = (String) charConstructor.construct(node);
                if (ch.length() == 0) {
                    result = null;
                } else if (ch.length() != 1) {
                    throw new YamlInstanceCreationException(ERROR_PREFIX + type, node.getStartMark(),
                            "Invalid node Character: '" + ch + "'; length: " + ch.length(), node.getStartMark());
                } else {
                    result = Character.valueOf(ch.charAt(0));
                }
            } else if (type == Float.class || type == Double.class || type == Float.TYPE
                    || type == Double.TYPE || type == BigDecimal.class) {
                if (type == BigDecimal.class) {
                    result = new BigDecimal(node.getValue());
                } else {
                    ConstructNode doubleConstructor = tagConstructors.get(Tag.FLOAT);
                    result = doubleConstructor.construct(node);
                    if (type == Float.class || type == Float.TYPE) {
                        result = Float.valueOf(((Double) result).floatValue());
                    }
                }
            } else if (type == Byte.class || type == Short.class || type == Integer.class
                    || type == Long.class || type == BigInteger.class || type == Byte.TYPE
                    || type == Short.TYPE || type == Integer.TYPE || type == Long.TYPE) {
                ConstructNode intConstructor = tagConstructors.get(Tag.INT);
                result = intConstructor.construct(node);
                if (type == Byte.class || type == Byte.TYPE) {
                    result = Byte.valueOf(result.toString());
                } else if (type == Short.class || type == Short.TYPE) {
                    result = Short.valueOf(result.toString());
                } else if (type == Integer.class || type == Integer.TYPE) {
                    result = Integer.parseInt(result.toString());
                } else if (type == Long.class || type == Long.TYPE) {
                    result = Long.valueOf(result.toString());
                } else {
                    // only BigInteger left
                    result = new BigInteger(result.toString());
                }
            } else if (Enum.class.isAssignableFrom(type)) {
                String enumValueName = node.getValue();
                try {
                    result = Enum.valueOf(type, enumValueName);
                } catch (Exception ex) {
                    throw new YamlInstanceCreationException(ERROR_PREFIX + type, node.getStartMark(),
                            "Unable to find enum value '" + enumValueName
                                    + "' for enum class: " + type.getName(), node.getStartMark());
                }
            } else if (Number.class.isAssignableFrom(type)) {
                //since we do not know the exact type we create Float
                ConstructYamlFloat contr = new ConstructYamlFloat();
                result = contr.construct(node);
            } else if (UUID.class == type) {
                result = UUID.fromString(node.getValue());
            } else {
                // when the class is unexpected then as the last resource let us try the tag
                if (tagConstructors.containsKey(node.getTag())) {
                    result = tagConstructors.get(node.getTag()).construct(node);
                } else {
                    throw new YamlInstanceCreationException(ERROR_PREFIX + type, node.getStartMark(),
                            "Unsupported class: " + type, node.getStartMark());
                }
            }
            return result;
        }
    }

    protected class ConstructMapping implements ConstructNode {
        private final ClassDefinition rootContext;
        private final PropertyUtils propertyUtils;

        public ConstructMapping(ClassDefinition rootContext) {
            this.rootContext = rootContext;
            this.propertyUtils = new PropertyUtils();
        }

        public Object construct(Node node) {
            MappingNode mnode = (MappingNode) node;
            Class<?> type = rootContext.getType();

            if (Map.class.isAssignableFrom(type)) {
                if (node.isRecursive()) {
                    return newCollectionInstance(type, mnode);
                } else {
                    return constructMapping(mnode);
                }
            } else if (Collection.class.isAssignableFrom(type)) {
                if (node.isRecursive()) {
                    return newCollectionInstance(type, mnode);
                } else {
                    return constructSet(mnode);
                }
            } else {
                Object obj = rootContext.newInstance(mnode);
                if (node.isRecursive()) {
                    return obj;
                } else {
                    return constructJavaBean2ndStep(mnode, obj);
                }
            }
        }


        protected Object newCollectionInstance(Class<?> type, Node node) {
            try {
                java.lang.reflect.Constructor<?> c = type.getDeclaredConstructor();
                c.setAccessible(true);
                return c.newInstance();
            } catch (NoSuchMethodException e) {
                throw new YamlInstanceCreationException(ERROR_PREFIX + rootContext.getType(), node.getStartMark(),
                        "NoSuchMethodException: " + e.getLocalizedMessage(), node.getStartMark(), e);
            } catch (Exception e) {
                throw new YamlInstanceCreationException(ERROR_PREFIX + rootContext.getType(), node.getStartMark(),
                        e.getLocalizedMessage(), node.getStartMark(), e);
            }
        }

        protected Object constructJavaBean2ndStep(MappingNode node, Object javabeanInstance) {
            flattenMapping(node);
            Class<? extends Object> beanType = rootContext.getType();
            List<NodeTuple> nodeValue = node.getValue();
            for (NodeTuple tuple : nodeValue) {
                ScalarNode keyNode;
                if (tuple.getKeyNode() instanceof ScalarNode) {
                    // key must be scalar
                    keyNode = (ScalarNode) tuple.getKeyNode();
                } else {
                    throw new YamlInstanceCreationException(ERROR_PREFIX + rootContext.getType(), node.getStartMark(),
                            "Keys must be scalars but found: " + tuple.getKeyNode(), tuple.getKeyNode().getStartMark());
                }
                Node valueNode = tuple.getValueNode();
                // keys can only be Strings
                if (keyNode.getNodeType() != NodeType.SCALAR) {
                    throw new YamlInstanceCreationException(ERROR_PREFIX + rootContext.getType(), node.getStartMark(),
                            "Keys must be scalars to create JavaBeans but found: " + keyNode.getNodeType(),
                            tuple.getKeyNode().getStartMark());
                }
                keyNode.setProperty(DEFINITION_KEY, new ClassDefinition(String.class));
                String key = (String) constructObject(keyNode);
                try {
                    Optional<ClassDefinition> contextOptional = classDefinitionRegistry.findByClass(beanType);
                    //TODO get property from ClassDefinition
                    Property property = getProperty(beanType, key);
                    if (!property.isWritable()) {
                        throw new YamlInstanceCreationException(ERROR_PREFIX + rootContext.getType(), node.getStartMark(),
                                "No writable property '" + key + "' on class: "
                                        + beanType.getName(), tuple.getKeyNode().getStartMark());
                    }
                    ClassDefinition valueContext = classDefinitionRegistry.findByClass(property.getType())
                            .orElse(new ClassDefinition(property.getType()));
                    valueNode.setProperty(DEFINITION_KEY, valueContext);
                    Object value = constructObject(valueNode);
                    property.set(javabeanInstance, value);
                } catch (DuplicateKeyException e) {
                    throw e;
                } catch (Exception e) {
                    throw new ConstructorException(
                            "Cannot create property=" + key + " for JavaBean=" + javabeanInstance,
                            node.getStartMark(), e.getMessage(), valueNode.getStartMark(), e);
                }
            }
            return javabeanInstance;
        }

        protected Property getProperty(Class<? extends Object> type, String name) {
            return propertyUtils.getProperty(type, name);
        }
    }
}

