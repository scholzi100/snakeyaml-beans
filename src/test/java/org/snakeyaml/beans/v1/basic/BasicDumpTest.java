/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.basic;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.snakeyaml.beans.v1.api.BeanDump;
import org.snakeyaml.engine.v2.api.DumpSettings;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("fast")
class BasicDumpTest {

    @Test
    @DisplayName("Dump the simplest JavaBean")
    void basicDump() {
        BasicBean bean = new BasicBean();
        bean.setAge(23);
        bean.setName("Bar");
        DumpSettings settings = DumpSettings.builder().build();
        BeanDump beanDump = new BeanDump(settings);
        String output = beanDump.dumpToString(bean);
        assertEquals("!!org.snakeyaml.beans.v1.basic.BasicBean {age: 23, name: Bar}\n", output);
    }
}
